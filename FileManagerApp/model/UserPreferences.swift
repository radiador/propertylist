//
//  UserPreferences.swift
//  FileManagerApp
//
//  Created by formador on 18/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import Foundation

struct UserPreferences: Codable {
    
    let darkBackgroudColor: Bool
}
