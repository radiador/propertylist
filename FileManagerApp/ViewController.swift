//
//  ViewController.swift
//  FileManagerApp
//
//  Created by formador on 18/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var darkModeSw: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let userPreferences = loadPreferences()
        
        darkModeSw.isOn = userPreferences.darkBackgroudColor
        
        darkModeSw.addTarget(self, action: #selector(ViewController.darkModeSwichAction), for: .valueChanged)
    }
    
    @IBAction func touchInSwichtAction(_ sender: UISwitch) {
        
        let userPreferences = UserPreferences(darkBackgroudColor: sender.isOn)
        
        DataManager.shared.saveUserPreferences(userPreferences)
        
        loadPreferences()
    }
    @objc private func darkModeSwichAction() {
        
        let userPreferences = UserPreferences(darkBackgroudColor: darkModeSw.isOn)
        
        DataManager.shared.saveUserPreferences(userPreferences)
        
        loadPreferences()
    }
    
    @discardableResult
    private func loadPreferences() -> UserPreferences {
        
        let userPreferences = DataManager.shared.loadUserPreferences()
        
        setPreferences(userPreferences: userPreferences)
        
        return userPreferences
    }
    
    private func setPreferences(userPreferences: UserPreferences) {
        
        if userPreferences.darkBackgroudColor {
            
            view.backgroundColor = .black
        } else {
            
            view.backgroundColor = .white
        }
    }
    
}

