//
//  DataManager.swift
//  FileManagerApp
//
//  Created by formador on 18/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import Foundation

class DataManager {
    
    static let shared: DataManager = DataManager()
    
    private init() {}
    
    func loadUserPreferences() -> UserPreferences {
        
        var userPreferences = loadUserPreferencesFromFile()
        
        if userPreferences == nil {
            
            userPreferences = loadUserPreferencesFromFile()
        }
        
        return userPreferences ?? UserPreferences(darkBackgroudColor: true)
    }
    
    func saveUserPreferences(_ userPreferences: UserPreferences) {
        
        let jsonEncoder = JSONEncoder()
        
        if let fileEncoded = try? jsonEncoder.encode(userPreferences), let url = userPreferencesUrlFile() {
            
            do {
                try fileEncoded.write(to: url, options: Data.WritingOptions.atomic)
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    private func loadDefaultUserPreferences() -> UserPreferences? {
        
        let plistDecoder = PropertyListDecoder()
        
        let urlFile = Bundle.main.url(forResource: "UserPreferences", withExtension: "plist")
        
        if let urlFile = urlFile, let data = try? Data(contentsOf: urlFile) {
            
            do {
               return try plistDecoder.decode(UserPreferences.self, from: data)
            } catch {
                print(error.localizedDescription)
                return nil
            }
        }
        
        return nil
    }
    
    private func loadUserPreferencesFromFile() -> UserPreferences? {
        
        let jsonDecoder = JSONDecoder()
        
        if let url = userPreferencesUrlFile(), let data = try? Data(contentsOf: url) {
            
            do {
                return try jsonDecoder.decode(UserPreferences.self, from: data)
            } catch {
                print(error.localizedDescription)
                return nil
            }
        }
        
        return UserPreferences(darkBackgroudColor: false)
    }
    
    private func userPreferencesUrlFile() -> URL? {

        //La carpeta Application support no existe por defecto en ios
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        
        if urls.count >= 1 {
            
            return urls[0].appendingPathComponent("userPreferences.json")
        }
        
        return nil
    }
}
